FROM alpine:edge

RUN apk --update --no-cache add alpine-sdk go libseccomp libseccomp-dev git

WORKDIR /root

RUN mkdir /root/gopath
ENV GOPATH /root/gopath

RUN go get github.com/opencontainers/runc
RUN mkdir -p /usr/local/sbin/
RUN cd /root/gopath/src/github.com/opencontainers/runc && \
        go build -buildmode=pie  -ldflags "-X main.gitCommit="bdbb9fab07fe879eb6b1a5dc4c96df21b7042ec3" -X main.version=1.0.0-rc5+dev " -tags "seccomp" -o runc . && \
        install -D -m0755 runc /usr/local/sbin/runc

RUN go get github.com/genuinetools/img
RUN cd /root/gopath/src/github.com/genuinetools/img/ && make build && make install && install -D -m0755 img /usr/local/sbin/img

RUN apk --update --no-cache add \
bash bash-completion \
git-bash-completion \
curl wget ncdu htop nmap nano \
pcre-dev libpcre32
